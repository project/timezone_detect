/**
 * @file
 * Determine and set user's timezone on page load.
 *
 * @todo: Use Drupal.behaviors?
 */
(function ($, Drupal, drupalSettings, once, jstz) {

  Drupal.behaviors.timezoneDetect = {
    attach: function (context, settings) {
      if (!once('timezone-detect', 'html').length) {
        return;
      }

      // Determine timezone from browser client using jsTimezoneDetect library.
      var tz = jstz.determine().name();

      // Not all timezones returned by jstz are valid in Drupal.
      // @see https://github.com/iansinnott/jstz/issues/23
      if (tz === 'Asia/Calcutta') {
        tz = 'Asia/Kolkata';
      }

      if (tz !== drupalSettings.timezone_detect.current_timezone) {

        // Post timezone to callback url via ajax.
        $.ajax({
          type: 'POST',
          url: drupalSettings.path.baseUrl + 'timezone-detect/ajax/set-timezone',
          dataType: 'json',
          data: {
            timezone: tz,
            token: drupalSettings.timezone_detect.token
          }
        });

        // Set any timezone select on this page to the detected timezone.
        $('select[name="timezone"] option[value="' + tz + '"]')
          .closest('select')
          .val(tz);
      }
    }

  };

})(jQuery, Drupal, drupalSettings, once, window.jstz);
